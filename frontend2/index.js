import io from 'socket.io-client'
import feathers from '@feathersjs/client'

// Create a websocket connecting to Feathers server
const socket = process.env.NODE_ENV === 'production' ? io() : io('localhost:3030')

const app = feathers()
app.configure(feathers.socketio(socket))

// app.configure(restClient.fetch(window.fetch.bind(window)))

async function main() {
   const usersList = await app.service('/api/users').find({})
   const ulElt = document.getElementById('list')
   for (const user of usersList) {
      const liElt = document.createElement('li')
      ulElt.appendChild(liElt)
      liElt.textContent = user.fullname
   }
}

main()
