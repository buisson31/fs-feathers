
async function main() {
   const response = await fetch('http://localhost:3030/api/users')
   const usersList = await response.json()
   const ulElt = document.getElementById('list')
   for (const user of usersList) {
      const liElt = document.createElement('li')
      ulElt.appendChild(liElt)
      liElt.textContent = user.fullname
   }
}

main()
