import './style.css'

import feathers from '@feathersjs/client'
import rest from '@feathersjs/rest-client'

const app = feathers()

const restClient = rest('http://localhost:3030')
app.configure(restClient.fetch(window.fetch.bind(window)))

async function main() {
   const usersList = await app.service('/api/users').find({})
   const ulElt = document.getElementById('list')
   for (const user of usersList) {
      const liElt = document.createElement('li')
      ulElt.appendChild(liElt)
      liElt.textContent = user.fullname
   }
}

main()
