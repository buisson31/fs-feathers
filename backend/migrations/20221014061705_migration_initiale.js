exports.up = function(knex) {

   return knex.schema
      .createTable('users', table => {
         table.increments('id').unsigned().primary()
         table.string('email').unique().notNullable()
         table.string('password')
         table.timestamp('created_at').defaultTo(knex.fn.now())
         table.string('fullname').notNull()
      })
      .createTable('pictures', table => {
         table.increments('id').unsigned().primary()
         table.integer('user_id').references('id').inTable('users').notNull().onDelete('cascade')
         table.timestamp('created_at').defaultTo(knex.fn.now())
         table.string('path').notNull()
      })
}

exports.down = function(knex) {
   return knex.schema
      .dropTable('users')
      .dropTable('pictures')
}
