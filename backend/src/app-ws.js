const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const socketio = require('@feathersjs/socketio')
const knex = require('knex')
const knexService = require('feathers-knex')

const dbConfig = require(`../knexfile.js`)
const database = knex(dbConfig.development)

// Create a feathers instance.
const app = express(feathers())
// Enable REST transport
app.configure(express.rest())
// JSON body parser middleware
app.use(express.json())

// Configure the Socket.io transport
app.configure(socketio({
   path: '/gallery-socket-io',
}))

// Create a channel that will handle the transportation of all realtime events
app.on('connection', connection => app.channel('everybody').join(connection))

// Publish all realtime events to the `everybody` channel
app.publish(() => app.channel('everybody'))


// Feathers/Knex REST services
app.use('users', knexService({ Model: database, name: 'users' }))
app.use('pictures', knexService({ Model: database, name: 'pictures' }))



// Start the server on port 3030
app.listen(3030, () => console.log('listening on port 3030'))
