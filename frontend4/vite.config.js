
import { defineConfig } from 'vite'

export default defineConfig({
   server: {
      port: 3000,
      open: true,
      proxy: {
         '^/gallery-socket-io/.*': {
            target: 'http://localhost:3030',
            ws: true,
            secure: false,
            changeOrigin: true,
         },
      }
   },
})
 