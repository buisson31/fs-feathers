import './style.css'

import feathers from '@feathersjs/client'
import io from 'socket.io-client'

const app = feathers()

// with no argument, connection is made with the server which served index.html
const socket = io({
   path: '/gallery-socket-io',
   transports: ["websocket"],
})
app.configure(feathers.socketio(socket))


function addUserElement(user) {
   const userElement = document.createElement('li')
   userElement.innerText = user.fullname
   document.body.appendChild(userElement)
}

// get & display already existing users
app.service('users').find({}).then(usersList => {
   usersList.forEach(user => addUserElement(user))
})

// Listen to new users being created
app.service('users').on('created', user => {
   console.log('CREATED', user)
   addUserElement(user)
})

const fullnameInput = document.getElementById('fullname')
const emailInput = document.getElementById('email')
const addButton = document.querySelector('button[type="submit"]')

addButton.addEventListener('click', ev => {
   // Create a new user
   app.service('users').create({
      fullname: fullnameInput.value,
      email: emailInput.value,
   })
   fullnameInput.value = ''
   emailInput.value = ''
})
